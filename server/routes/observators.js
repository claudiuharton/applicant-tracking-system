
const express = require('express');
const router = express.Router();
let observatorController = require('../controllers/observators');

router.get('/', observatorController.findAll);

router.get('/:id', observatorController.findById);
router.put('/:id', observatorController.updateById);





module.exports = router;