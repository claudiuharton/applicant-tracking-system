
const express = require('express');
const router = express.Router();
let questionController = require('../controllers/questions')



router.get('/:p_id/questions',questionController.findByPositionId);

router.get('/:p_id/questions/:q_id', questionController.findByQuestionId);

router.post('/:p_id/questions', questionController.createByPositionId);

router.put('/:p_id/questions/:q_id',questionController.updateByQuestionId);

router.delete('/:p_id/questions/:q_id', questionController.deleteByQuestionId);






module.exports = router;