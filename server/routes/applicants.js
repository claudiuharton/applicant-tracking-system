
const express = require('express');
const router = express.Router();
let applicantsController = require('../controllers/applicants');

router.get('/', applicantsController.findAll);

router.get('/:id', applicantsController.findById);
router.put('/:id', applicantsController.updateById);





module.exports = router;