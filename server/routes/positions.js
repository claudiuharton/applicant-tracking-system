
const express = require('express');
const router = express.Router();
let positionController = require('../controllers/positions');

router.get('/', positionController.findAll);
router.post('/',positionController.create);

router.get('/:id', positionController.findById);
router.put('/:id', positionController.updateById);
router.delete('/:id', positionController.deleteById);






module.exports = router;