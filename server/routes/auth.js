const passport = require('passport');
const express = require('express');
const router = express.Router();


router.get('/logout', (req,res) => {
  req.logout();
  res.redirect('/');
});



router.get('/google',passport.authenticate('google',{
    scope:['profile','email']
}));


router.get('/google/redirect',passport.authenticate('google'),(req,res)=>{
  //      res.status(200).send(req.user);

   res.redirect('http://localhost:8080/profile');
});





module.exports = router;