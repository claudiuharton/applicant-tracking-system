const express = require('express');
const router = express.Router();

const defaultController = require('../controllers/default');


router.get('/', (req, res) => {
  res.status(200).send('<h1>here you are</h1>');
});

router.get('/create', defaultController.recreateTables);
router.get('/tests', defaultController.recreateTests);
router.use('/users', require('./users'));
router.use('/observators', require('./observators'));
router.use('/applicants', require('./applicants'));
router.use('/positions', require('./positions'));
router.use('/positions', require('./questions'));


router.use('/auth', require('./auth'));


module.exports = router;






