
const express = require('express');
const router = express.Router();
let usersController = require('../controllers/users');

router.get('/', usersController.findAll);
router.post('/', usersController.create);

router.get('/:id', usersController.findById);
router.delete('/:id', usersController.deleteById);




module.exports = router;