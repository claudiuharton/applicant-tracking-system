'use strict';


module.exports = function (sequelize, DataTypes) {
    return sequelize.define('applicant_position', {
        testGrades: DataTypes.STRING,
    }, {
        underscored: true
    });

};