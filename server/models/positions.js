'use strict';


module.exports = function (sequelize, DataTypes) {
    return sequelize.define('position', {
         title: DataTypes.STRING,
         description:DataTypes.STRING

    }, {
        underscored: true
    });

};