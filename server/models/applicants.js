'use strict';


module.exports = function (sequelize, DataTypes) {
    return sequelize.define('applicant', {
        firstName: DataTypes.STRING,
        lastName: DataTypes.STRING,
        email: DataTypes.STRING,
        cv_filename: DataTypes.STRING,
    }, {
        underscored: true
    });

};