'use strict';


module.exports = function (sequelize, DataTypes) {
    return sequelize.define('user', {
        googleId: DataTypes.STRING,
        email: DataTypes.STRING,
        type: DataTypes.INTEGER
    }, {
        underscored: true
    });

};