'use strict';


module.exports = function (sequelize, DataTypes) {
    return sequelize.define('interview', {
        location: DataTypes.STRING,
        date:DataTypes.DATE
    }, {
        underscored: true
    });

};