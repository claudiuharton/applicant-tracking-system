'use strict';


module.exports = function (sequelize, DataTypes) {
    return sequelize.define('observator_interviews', {
        feedback: DataTypes.STRING
    }, {
        underscored: true
    });

};