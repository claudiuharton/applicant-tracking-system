'use strict';


module.exports = function (sequelize, DataTypes) {
    return sequelize.define('observator', {
        firstName: DataTypes.STRING,
        lastName: DataTypes.STRING,
        email: DataTypes.STRING

    }, {
        underscored: true
    });

};