'use strict';
let sequelize = require('../conf/db');

let User = sequelize.import('./users');
let Question = sequelize.import('./questions');
let Position_Questions = sequelize.import('./position_questions');
let Position = sequelize.import('./positions');
let Observator = sequelize.import('./observators');
let Observator_Interviews = sequelize.import('./observator_interviews');
let Interview = sequelize.import('./interviews');
let Applicant = sequelize.import('./applicants');
let Applicant_Positions = sequelize.import('./applicant_positions');


Applicant.belongsTo(User, { onDelete: 'cascade' });
Applicant.belongsTo(Interview, { onDelete: 'cascade' });

Observator.belongsTo(User,  { onDelete: 'cascade' });

Observator_Interviews.belongsTo(Interview, { onDelete: 'cascade' });
Observator_Interviews.belongsTo(Observator, { onDelete: 'cascade' });

Applicant_Positions.belongsTo(Applicant, { onDelete: 'cascade' });
Applicant_Positions.belongsTo(Position, { onDelete: 'cascade' });

Position_Questions.belongsTo(Position, { onDelete: 'cascade' });
Position_Questions.belongsTo(Question, { onDelete: 'cascade' });

Position.hasMany(Question,{onDelete:'cascade'});



module.exports = {
  User,
  Question,
  Position,
  Position_Questions,
  Observator,
  Observator_Interviews,
  Interview,
  Applicant,
  Applicant_Positions,
  sequelize
};
