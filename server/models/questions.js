'use strict';


module.exports = function (sequelize, DataTypes) {
    return sequelize.define('question', {
        text: DataTypes.TEXT,
        numberAnswers: DataTypes.INTEGER,
        numberValidAnswers:DataTypes.INTEGER,
        answersText: DataTypes.TEXT,
        answer: DataTypes.STRING
    }, {
        underscored: true
    });

};