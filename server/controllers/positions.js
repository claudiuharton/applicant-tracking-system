let Position = require('../models').Position;

module.exports.findAll = (req, res) => {
  Position.findAll()
      .then((results) => {
        res.status(200).json(results)
      })
      .catch(() => res.status(500).send('hm, that was unexpected...'))
}


module.exports.create =(req, res) => {
  Position.create({title:req.body.title,description:req.body.description })
      .then(() => {
        res.status(201).send('created')
      })
      .catch(() => res.status(500).send('hm, that was unexpected...'))
}


module.exports.findById = (req, res) => {
  Position.findById(req.params.id)
      .then((result) => {
        if (result){
          res.status(200).json(result)
        }
        else{
          res.status(404).send('not found')
        }
      })
      .catch(() => res.status(500).send('hm, that was unexpected...'))
}

module.exports.updateById =  (req, res) => {
  Position.findById(req.params.id)
      .then((result) => {
        if (result){
          return result.update({title:req.body.title || result.title, description:req.body.description || result.description})
        }
        else{
          res.status(404).send('not found')
        }
      })
      .then(() => {
        res.status(201).send('modified')
      })
      .catch(() => res.status(500).send('hm, that was unexpected...'))
}

module.exports.deleteById = (req, res) => {
  Position.findById(req.params.id)
      .then((result) => {
        if (result){
          return result.destroy()
        }
        else{
          res.status(404).send('not found')
        }
      })
      .then(() => {
        res.status(201).send('removed')
      })
      .catch(() => res.status(500).send('hm, that was unexpected...'))
}