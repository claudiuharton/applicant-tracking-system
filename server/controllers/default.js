const fs = require('fs');

const model = require('../models');


module.exports.recreateTables = (req, res) => {
  model.sequelize.sync({force: true})
      .then(() => res.status(201).send('recreated all tables'))
      .catch(() => res.status(500).send('hm, that was unexpected...'))

};


module.exports.recreateTests = async (req, res) => {

  fs.readdir('Tests', (err, files) => {

    files.map((file) => {
      fs.readFile(file, 'utf8', (err, contents) => {
        console.log(contents);
      })

    })


  }).then(() => res.status(200).send('tests updated'))
      .catch(() => res.status(500).send('hm, that was unexpected...'));


};
