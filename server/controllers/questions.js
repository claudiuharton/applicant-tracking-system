let Question = require('../models').Question;
let Position = require('../models').Position;

module.exports.findByPositionId = (req, res) => {
    Position.findById(req.params.p_id)
        .then((result) => {

            if (result) {
                return result.getQuestions();
            }
            else {
                res.status(404).send('not found')
            }
        })
        .then((results) => {
            console.log(results);
            res.status(200).json(results)
        })
        .catch((error) => res.status(500).send(error))
}

module.exports.findByQuestionId = (req, res) => {
    Position.findById(req.params.p_id)
        .then((result) => {
            if (result) {
                return result.getQuestions({where: {id: req.params.q_id}})
            }
            else {
                res.status(404).send('not found')
            }
        })
        .then((result) => {
            if (result) {
                res.status(200).json(result)
            }
            else {
                res.status(404).send('not found')
            }
        })
        .catch(() => res.status(500).send('hm, that was unexpected...'))
}

module.exports.createByPositionId = (req, res) => {
    Position.findById(req.params.p_id)
        .then((result) => {
            if (result) {
                let question = req.body;
                question.position_id = result.id;
                return Question.create(question)
            }
            else {
                res.status(404).send('not found')
            }
        })
        .then(() => {
            res.status(201).json('created')
        })
        .catch((err) => res.status(500).send('hm, that was unexpected...'))
}

module.exports.updateByQuestionId = (req, res) => {
    Question.findById(req.params.q_id)
        .then((result) => {
            if (result) {
                return result.update(req.body)
            }
            else {
                res.status(404).send('not found')
            }
        })
        .then(() => {
            res.status(201).send('modified')
        })
        .catch(() => res.status(500).send('hm, that was unexpected...'))
}

module.exports.deleteByQuestionId = (req, res) => {
    Question.findById(req.params.q_id)
        .then((result) => {
            if (result) {
                return result.destroy()
            }
            else {
                res.status(404).send('not found')
            }
        })
        .then(() => {
            res.status(201).send('removed')
        })
        .catch(() => res.status(500).send('hm, that was unexpected...'))
}