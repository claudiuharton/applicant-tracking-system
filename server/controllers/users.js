const User = require('../models').User;
const Observator = require('../models').Observator;
const Applicant = require('../models').Applicant;


module.exports.findAll = (req, res) => {

    User.findAll()
        .then((results) => {

            let jsonResults = [];


            Array.from(results).map(async (item, index) => {
                jsonResults.push(item.toJSON());

                let result;

                switch (parseInt(jsonResults[index].type)) {

                    case 0:
                        result = await Applicant.findAll({
                                where: {
                                    user_id: jsonResults[index].id
                                }
                            }
                        );


                        break;
                    case 101:
                        result = await Observator.findAll({
                                where: {
                                    user_id: jsonResults[index].id
                                }
                            }
                        )


                        break;

                }
                jsonResults[index].firstName = result[0].firstName;
                jsonResults[index].lastName = result[0].lastName;


                if (index === jsonResults.length - 1) {
                    res.status(200).send(jsonResults);
                }


            });


            if (results.length === 0) {
                res.status(404).send("Nothing here");
            }

        }).catch((err) => {
        res.status(500).send(err);
    })
}


module.exports.create = (req, res) => {
    User.findAll({
        where: {
            email: req.body.email
        }
    }).then((result) => {
        if (result.length === 0) {
            User.create({
                googleId: req.body.googleId,
                email: req.body.email,
                type: req.body.type
            }).then((user) => {
                switch (parseInt(user.type)) {


                    case 0:
                        Applicant.create({

                            firstName: req.body.firstName,
                            lastName: req.body.lastName,
                            email: req.body.email,
                            user_id: user.id,
                            cv_filename: req.body.cv_filename
                        }).then((applicant) => {
                            res.status(201).send(`Applicant [${applicant.firstName} ${applicant.lastName}: ${applicant.email}] created.`)
                        })
                            .catch(() => res.status(500).send(`Applicant [${user.email}] couldn't be created.`));
                        break;
                    case 101:
                        Observator.create({
                            user_id: user.id,
                            firstName: req.body.firstName,
                            lastName: req.body.lastName,
                            email: req.body.email,

                        }).then((observator) => {
                            res.status(201).send(`Observator [${observator.firstName} ${observator.lastName}: ${observator.email}] created.`)
                        })
                            .catch(() => res.status(500).send(`Observator [${user.email}] couldn't be created.`));
                        break;
                    default:
                        res.status(500).send(`Invalid type for [${user.email}].`);


                }
            }).catch(() => res.status(500).send(`User [${user.email}] couldn't be created`));
        } else {
            res.status(500).send(`Already existing email: [${req.body.email}].`);
        }


    });


};


module.exports.findById = (req, res) => {
    User.findById(req.params.id)
        .then((result) => {
            if (result) {
                res.status(200).json(result)
            }
            else {
                res.status(404).send('not found')
            }
        })
        .catch(() => res.status(500).send('hm, that was unexpected...'))
}


module.exports.deleteById = async (req, res) => {

    let user = await User.findById(req.params.id);
    if (user) {
        return user.destroy()
            .then((user) => {
                res.status(201).send(`User ${user.email} removed`)
            })
            .catch(() => res.status(500).send('hm, that was unexpected...'))
    }
    else {
        res.status(404).send('not found')
    }


};


module.exports.createIntern = (req) => {
    User.findAll({
        where: {
            googleId: req.body.googleId
        }
    })


};


module.exports.findOrCreateByGoogleId = (req) => {

};


