let Observator = require('../models').Observator;
let User = require('../models').User;

module.exports.findAll = (req, res) => {
  Observator.findAll()
      .then((results) => {

        res.status(200).json(results);
      })
      .catch(() => res.status(500).send('Internal server error.'));
};


module.exports.findById = (req, res) => {
  Observator.findById(req.params.id)
      .then((result) => {
        if (result) {
          res.status(200).json(result)
        }
        else {
          res.status(404).send('not found')
        }
      })
      .catch(() => res.status(500).send('hm, that was unexpected...'))
}

module.exports.updateById = async (req, res) => {

  let observator = await  Observator.findById(req.params.id);

  if (observator) {
    observator.update({
      firstName: req.body.firstName || observator.firstName,
      lastName: req.body.lastName || observator.lastName,
      email: req.body.email ||observator.email
    })
        .catch((err) => res.status(500).send(err))
  } else {
    res.status(404).send('not found')
  }


  let user = await User.findById(observator.user_id);


  if (user) {
    user.update({
      email: req.body.email || user.email,
    })
        .catch((err) => res.status(500).send(err))
  }
  else {
    res.status(404).send('not found')
  }

  res.status(201).send(`User ${user.email} updated.`)

}

