let Applicant = require('../models').Applicant;
let User = require('../models').User;

module.exports.findAll = (req, res) => {
  Applicant.findAll()
      .then((results) => {


        res.status(200).json(results)
      })


      .catch(() => res.status(500).send('Internal server error.'))
}




module.exports.findById = (req, res) => {
  Applicant.findById(req.params.id)
      .then((result) => {
        if (result) {
          res.status(200).json(result)
        }
        else {
          res.status(404).send('not found')
        }
      })
      .catch(() => res.status(500).send('hm, that was unexpected...'))
}





module.exports.updateById = async (req, res) => {

  let applicant = await  Applicant.findById(req.params.id);

  if (applicant) {
    applicant.update({
      firstName: req.body.firstName || applicant.firstName,
      lastName: req.body.lastName || applicant.lastName,
      email: req.body.email || applicant.email,
      cv_filename:req.body.cv_filename || applicant.cv_filename
    })
        .catch((err) => res.status(500).send(err))
  } else {
    res.status(404).send('not found')
  }


  let user = await User.findById(applicant.user_id);


  if (user) {
    user.update({
      email: req.body.email || user.email,
    })
        .catch((err) => res.status(500).send(err))
  }
  else {
    res.status(404).send('not found')
  }

  res.status(201).send(`User ${user.email} updated.`)
};


