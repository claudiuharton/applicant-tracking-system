const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const key = require('./keys');

const User = require('../models').User;
const Observator = require('../models').Observator;
const Applicant = require('../models').Applicant;


passport.serializeUser((user, done) => {
    done(null, user.googleId);
});

passport.deserializeUser((googleId, done) => {
    User.findAll({
        where: {
            googleId: googleId
        }
    }).then((user) =>{
        done(null,user);
    });

});


passport.use(
    new GoogleStrategy({
            clientID: key.google.clientID,
            clientSecret: key.google.clientSecret,
            callbackURL: `/api/auth/google/redirect`
        }, (accessToken, refreshToken, profile, done) => {


            const req = {
                body: {
                    googleId: profile.id,
                    email: profile.emails[0].value,
                    type: 0,
                    firstName: profile.name.givenName,
                    lastName: profile.name.familyName
                }
            }


            User.findAll({
                where: {
                    googleId: req.body.googleId
                }
            }).then((result) => {
                if (result.length === 0) {
                    User.create({
                        googleId: req.body.googleId,
                        email: req.body.email,
                        type: req.body.type
                    }).then((user) => {
                        switch (parseInt(user.type)) {


                            case 0:
                                Applicant.create({

                                    firstName: req.body.firstName,
                                    lastName: req.body.lastName,
                                    email: req.body.email,
                                    user_id: user.id,
                                    cv_filename: req.body.cv_filename
                                }).then((applicant) => {
                                    console.log(`Applicant [${applicant.firstName} ${applicant.lastName}: ${applicant.email}] created.`);
                                    done(null, user);
                                })
                                    .catch(() => console.log(`Applicant [${user.email}] couldn't be created.`));
                                break;
                            case 101:
                                Observator.create({
                                    user_id: user.id,
                                    firstName: req.body.firstName,
                                    lastName: req.body.lastName,
                                    email: req.body.email,

                                }).then((observator) => {
                                    console.log(`Observator [${observator.firstName} ${observator.lastName}: ${observator.email}] created.`);
                                    done(null, user);
                                })
                                    .catch(() => console.log(`Observator [${user.email}] couldn't be created.`));
                                break;
                            default:
                                console.log(`Invalid type for [${user.email}].`);



                        }
                    }).catch(() => console.log(`User [${user.email}] couldn't be created`));

                }

                if (result.length === 1) {
                    console.log(`current user is ${result[0].email}`);
                    done(null, result[0])
                }


            });


        }
    ));