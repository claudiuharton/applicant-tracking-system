const express = require('express');
const bodyParser = require('body-parser');
const router = require('./routes');
const keys = require('./conf/keys');
const passportSetup = require('./conf/auth');
const cookieSession = require('cookie-session');
const passport = require('passport');
const path = require('path');

const PORT = 8080;



const app = express();



app.use(cookieSession({
  maxAge: 24 * 60 * 60 * 1000,
  keys: [keys.session.cookieKey]
}));

app.use(passport.initialize());
app.use(passport.session());


app.use(bodyParser.json());

app.use('/api', router);

// app.get('/', (req, res) => {
//   res.sendFile(path.resolve(__dirname, '../.', 'client/build', 'index.html'));
// });
//
// app.use(express.static(path.resolve(__dirname, '..', 'client/build')));
app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}.`);
});
