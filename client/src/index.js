import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import {BrowserRouter, Route} from "react-router-dom";

import AuthExample from "./components/App";
import HomeComponent from "./components/home/HomeComponent";
import PositionComponent from "./components/position/PositionComponent";
import PositionListComponent from "./components/position/PositionListComponent";
import PositionNewComponent from "./components/position/PositionNewComponent";
import PositionEditComponent from "./components/position/PositionEditComponent";

ReactDOM.render(
    <BrowserRouter>
            <div className="row">
                <div className="col-sm-8 col-sm-offset-2">
                    <nav className="navbar navbar-default">
                        <div className="container-fluid">
                            <div className="navbar-header">
                                <a className="navbar-brand">Simple CRUD</a>
                            </div>
                            <div id="navbar" className="navbar-collapse collapse">
                                <ul className="nav navbar-nav">
                                    <li><a href="/">Home</a></li>
                                    <li><a href="/positions">Positions Management</a></li>

                                    <li><a href="/auth">Authentication</a></li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                    <div>
                        <Route path="/auth" component={AuthExample}/>


                    </div>
                    <Route path="/" component={HomeComponent}/>
                    <Route path="/positions" component={PositionComponent}/>
                    <Route path="/positions/list" component={PositionListComponent}/>
                    <Route path="/positions/new" component={PositionNewComponent}/>
                    <Route path="/positions/edit/:positionId" component={PositionEditComponent}/>



                </div>
            </div>
    </BrowserRouter>,
    document.getElementById('app'));
registerServiceWorker();
