import axios from 'axios';

const BASE_URL = 'http://localhost:8080';

export {getUsers, getPositions};

function getUsers() {
    const url = `${BASE_URL}/api/users`;
    return axios.get(url).then(response => response.data);
}

function getPositions() {
    const url = `${BASE_URL}/api/positions`;
    return axios.get(url).then(response => response.data);
}