import React from 'react';
import axios from 'axios';
import toastr from 'toastr';


class ProductNewComponent extends React.Component{

	constructor(props) {
		super(props);
		this.state = {
			title : "",
			description : ""
		}
	}

	submitForm(event){
		event.preventDefault();
		toastr.clear();
		var isError = false;
		if(this.state.title === ""){
			toastr.error("Title must be filled!");
			isError=true;
		}
		if(this.state.description === ""){
			toastr.error("Description must be filled!");
			isError=true;
		}
		if(!isError){
			toastr.info('Inserting new position data...');
			axios.post('http://localhost:8080/api/positions',{
				title : this.state.title,
				description : this.state.description
			}).then(function(response){
				toastr.clear();
				window.location.href = "http://localhost:8080/positions/list";
			}).catch(function(error){
				toastr.clear();
				toastr.error(error);
			});
		}
	}

	onTitleNameChange(e){
		this.setState({
			id : this.state.id,
			title : e.target.value.trim(),
			description : this.state.description
		});
	}

	onDescriptionChange(e){
		this.setState({
			id : this.state.id,
			title : this.state.title,
			description : e.target.value
		});
	}

	render(){
		return (
			<div>
				<form className="form-horizontal" onSubmit={this.submitForm.bind(this)}>
				    <div className="form-group">
				        <label className="control-label col-sm-2" >Title: </label>
				        <div className="col-sm-10">
				            <input 	type="text"
				            		onChange={this.onTitleNameChange.bind(this)}
				            		className="form-control" placeholder="Title" />
				        </div>
				    </div>
				    <div className="form-group">
				        <label className="control-label col-sm-2">Description: </label>
				        <div className="col-sm-10">
				            <input 	type="text"
				            		onChange={this.onDescriptionChange.bind(this)}
				            		 className="form-control" placeholder="Description" />
				        </div>
				    </div>
				    <div className="form-group">
				        <div className="col-sm-offset-2 col-sm-10">
				            <button type="submit" className="btn btn-default">Save</button>
				        </div>
				    </div>
				</form>

			</div>
		);
	}

}

export default ProductNewComponent;