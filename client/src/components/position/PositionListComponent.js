import React from 'react';
import axios from 'axios';
import toastr from 'toastr';

import TableClass from '../../classes/TableClass';
import ReactConfirmAlert from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css



class PositionListComponent extends React.Component {


    constructor(props) {
        super(props);
        this.getPositionList();

        this.state = {
            cols: [
                {key: 'dataHeaderId', label: 'Id'},
                {key: 'dataHeaderTitle', label: 'Title'},
                {key: 'dataHeaderDescription', label: 'Description'},

            ],
            data: [],
            showDialog: false
        };
    }

    getPositionList() {
        toastr.info('Fetching positions list...');
        var self = this;
        axios.get('http://localhost:8080/api/positions').then(function (response) {
            toastr.clear();
            self.setState({
                cols: self.state.cols,
                data: response.data
            });


        }).catch(function (error) {
            toastr.clear();
            toastr.error(error);
        });
    }

    updatePosition(data) {


        const id = data.target.getAttribute('data-id');

        window.location.href = 'http://localhost:8080/positions/edit/' + id;
    }

    deletePosition(data) {
        const id = data.target.getAttribute('data-id');


        axios.delete('http://localhost:8080/api/positions/'+id).then(function (response) {
            toastr.clear();
            toastr.info('Item deleted...');
            window.location.href = 'http://localhost:8080/positions/list';

        }).catch(function (error) {
            toastr.clear();
            toastr.error(error);
        });
    }


    onOpenModal = () => {
        this.setState({ showDialog: true });
    };

    onCloseModal = () => {
        this.setState({ showDialog: false });
    };

    render() {
        return (
            <div>
                <TableClass cols={this.state.cols} data={this.state.data} onDelete={() => this.deletePosition(this.state.data)   }
                            onUpdate={this.updatePosition}/>


                <div>
                    {
                        this.state.showDialog &&
                        <ReactConfirmAlert
                            title="Confirm to delete"
                            message="Are you sure to do this."
                            confirmLabel="Confirm"
                            cancelLabel="Cancel"

                            onConfirm={() => this.deletePosition()}
                            onCancel={() => this.onCloseModal()}
                        />
                    }
                </div>


            </div>
        );
    }

}

export default PositionListComponent;