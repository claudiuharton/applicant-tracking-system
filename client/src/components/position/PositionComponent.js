import React from 'react';
class ProductComponent extends React.Component{

	render(){
		return (
			<div className="panel panel-default">
			  	<div className="panel-heading">Positions Management</div>
			  	<div className="panel-body">
					<ul className="nav nav-tabs">

                      <li><a href="/positions/list">Positions List</a></li>
                      <li><a href="/positions/new">Add New positions</a></li>

					</ul>
					<br />
					{this.props.children}
			  	</div>
			</div>
		);
	}

}

export default ProductComponent;