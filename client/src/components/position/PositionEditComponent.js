import React from 'react';
import axios from 'axios';
import toastr from 'toastr';

class PositionEditComponent extends React.Component{

	constructor(props) {
		super(props);
		this.state = {
			id : '',
			title : '',
			description : ''
		};
		console.log(this.params)
		this.getData(this);
	}

	getData(self){


      let pathArray = window.location.pathname.split( '/' );

		toastr.info("Fetching product data...");
		axios.get('http://localhost:8080/api/positions/' + pathArray[pathArray.length -1]).then(function(response){
			toastr.clear();

          self.setState({
            id : response.data.id,
            title : response.data.title,
            description : response.data.description
          });

			document.querySelector(`#jobId`).value = self.state.id;
			document.querySelector(`#jobTitle`).value = self.state.title;
			document.querySelector(`#jobDescription`).value = self.state.description;



		}).catch(function(error){
			toastr.clear();
			toastr.error(error);
		});
	}

	submitForm(event){
      event.preventDefault();
      toastr.clear();
      var isError = false;
      if(this.state.title === ""){
        toastr.error("Title must be filled!");
        isError=true;
      }
      if(this.state.description === ""){
        toastr.error("Description must be filled!");
        isError=true;
      }
      if(!isError){
        toastr.info("Updating position data...");
        console.log (this.state);
        axios.post('http://localhost:8080/api/positions/'+this.state.id,{

          title : this.state.title,
          description : this.state.description
        }).then(function(response){
          toastr.clear();
          window.location.href = "http://localhost:8080/product/list";
        }).catch(function(error){
          toastr.clear();
          toastr.error(error);
        });
      }
	}

	onPositionIdChange(e){
		this.setState({
			id : e.target.value,
			title : this.state.title,
			description : this.state.description
		});
	}

	onPositionTitleChange(e){
		this.setState({
			id : this.state.id,
			title : e.target.value.trim(),
          description : this.state.description
		});
	}

	onPositionDescriptionChange(e){
		this.setState({
			id : this.state.id,
          title : this.state.title,
			description : e.target.value
		});
	}

	render(){
		return (
			<div>
				<form classTitle="form-horizontal" onSubmit={this.submitForm.bind(this)}>
				    <div classTitle="form-group">
				        <label classTitle="control-label col-sm-2" htmlFor="productId">Id: </label>
				        <div classTitle="col-sm-10">
				            <input 	type="number"
				            		onChange={this.onPositionIdChange.bind(this)}
				            		id="jobId" classTitle="form-control" placeholder="Position Id" disabled="disabled" />
				        </div>
				    </div>
				    <div classTitle="form-group">
				        <label classTitle="control-label col-sm-2" >Title : </label>
				        <div classTitle="col-sm-10">
				            <input 	type="text"
				            		onChange={this.onPositionTitleChange.bind(this)}
				            		id="jobTitle" classTitle="form-control" placeholder="Title" />
				        </div>
				    </div>
				    <div classTitle="form-group">
				        <label classTitle="control-label col-sm-2" htmlFor="productDescription">Description: </label>
				        <div classTitle="col-sm-10">
				            <input 	type="text"
				            		onChange={this.onPositionDescriptionChange.bind(this)}
				            		id="jobDescription" classTitle="form-control" placeholder="Description" />
				        </div>
				    </div>
				    <div classTitle="form-group">
				        <div classTitle="col-sm-offset-2 col-sm-10">
				            <button type="submit" classTitle="btn btn-default">Save Modification</button>
				        </div>
				    </div>
				</form>

			</div>
		);
	}

}

export default PositionEditComponent;