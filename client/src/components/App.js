import React, {Component} from 'react';
import logo from '../logo.svg';
import './App.css';
import GoogleButton from 'react-google-button'
import {
    BrowserRouter as Router,
    Route,
    Link,
    Redirect,
    withRouter
} from 'react-router-dom'
import AppBarExempleIcon from "./AppBarExempleIcon";

const AuthExample = () => (
    <Router>
        <div className="App">
            <header className="App-header">


                <img src={logo} className="App-logo" alt="logo"/>
                <h1 className="App-title">Enter using your Google Account.</h1>


            </header>
            <AuthButton/>
            <ul>
                <li><Link to="/user">Protected Page</Link></li>
            </ul>
            <Route path="/login" component={Login}/>
            <PrivateRoute path="/user" component={Protected}/>
        </div>
    </Router>
)

const fakeAuth = {
    isAuthenticated: false,
    authenticate(cb) {
        this.isAuthenticated = true
        setTimeout(cb, 100) // fake async
    },
    signout(cb) {
        this.isAuthenticated = false
        setTimeout(cb, 100)
    }
}

const PrivateRoute = ({component: Component, ...rest}) => (
    <Route {...rest} render={props => (
        fakeAuth.isAuthenticated ? (
            <Component {...props}/>
        ) : (
            <Redirect to={{
                pathname: '/login',
                state: {from: props.location}
            }}/>
        )
    )}/>
)


const Protected = () => <h3>Protected</h3>


const AuthButton = withRouter(({history}) => (
    fakeAuth.isAuthenticated ? (
        <p>
            Welcome! <button onClick={() => {
            fakeAuth.signout(() => history.push('/'))
        }}>Sign out</button>
        </p>
    ) : (
        <p>You are not logged in.</p>
    )
))


class Login extends Component {
    state = {
        redirectToReferrer: false
    }

    login = () => {
        fakeAuth.authenticate(() => {
            this.setState({redirectToReferrer: true})
        })
    }

    render() {
        const {from} = this.props.location.state || {from: {pathname: '/'}}
        const {redirectToReferrer} = this.state
        if (redirectToReferrer) {
            return (
                <Redirect to={from}/>
            )
        }

        return (

           <div>
                <div className="Content">
                    <GoogleButton
                        onClick={() => {
                            window.location.href = ('http://localhost:8080/api/auth/google')
                        }}
                    />


                </div>
                <nav className="navbar navbar-light">
                    <ul className="nav navbar-nav">
                        <div>
                            <p>You must log in to view the page at {from.pathname}</p>
                            <button onClick={this.login}>Log in</button>
                        </div>
                    </ul>
                </nav>
           </div>
        );
    }
}

export default AuthExample;